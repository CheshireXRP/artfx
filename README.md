# ARTFX

UNIQUE XRPL NFT MARKETS

**Question:**
- Can we build the world's first full-stack XRPL enabled digital bazar by linking virtual worlds  - and the objects within them - with existing payment technology, so that data and payments can move together within the context of copyrights online?
- Should this marketplace empower artists to collaborate and create by challenging existing payment models and optimizing interaction in digital spaces?
- How should value move in pure utilitarian systems?

**Proposal:**

Construct the digital platform ART.FX [XRP.ART.FX]; pronounced, 'Artifacts', the platform is focused on innovating payments for digital artists and collectives.

- Service individual artists with large market reach
- Create white label XRPL solutions for their art marketplaces (webstores)
- Interoperable data+payments so webstores can 'interact'
- Layer webstores into XR spaces (Hubs) add experimental payment options (Stripe)

---

**What are NFTs:**

A non-fungible token (NFT) is a special type of cryptographic token which represents something unique; non-fungible tokens are thus not mutually interchangeable. This is in contrast to eg. the native asset on the XRPL, XRP, where XRP can be sent and received without being uniquely (per token) identified.

Non-fungible tokens are used to create verifiable digital scarcity, as well as digital ownership, and the possibility of asset interoperability across multiple platforms. NFTs are used in several specific applications that require unique digital items like crypto art, digital collectibles, and online gaming.

This project proposes to build XRPL enabled webstores to sell digital content produced by artists with XRPL enabled payment processing and engagement tracking.

**XRPL NFT Standards:**

[Wallet based Proof of Digital Asset Property and Rights (NFT) #18 · Discussion #40 · XRPLF/XRPL-Standards](https://github.com/XRPLF/XRPL-Standards/discussions/40)

[0014 XLS-14d: Non fungible tokens (indivisible NFT's) on the XRPL · Discussion #30 · XRPLF/XRPL-Standards](https://github.com/XRPLF/XRPL-Standards/discussions/30)

**XRP Engagement Payments:**

[clever-gallery](https://github.com/clever-gallery)

**ILP-Stripe:**

[interledger-deprecated/ilp-plugin-stripe](https://github.com/interledger-deprecated/ilp-plugin-stripe)

**Hubs + Coil:**

[Innovating on Web Monetization: Coil and Firefox Reality - Mozilla Hacks - the Web developer blog](https://hacks.mozilla.org/2020/03/web-monetization-coil-and-firefox-reality/)

[Hubs Cloud Personal](https://aws.amazon.com/marketplace/pp/prodview-2kj77cx5ya7du)

**DEX + Gateway:**

[Stats - xrplcluster.com](https://ws-stats.com/)

**Paystring CRM + Payments:**

[Open Source, Universal Payment Identifier](https://paystring.org/)

## Project Proposal Demonstration:

[RippleX Developer - NFTs! Come and see about NFTs on the XRP Ledger - RippleXDev on Twitch](https://www.twitch.tv/videos/960265121)

## Proof of Concepts:

- [https://twitter.com/calcs9/status/1371927192817569798?s=20](https://twitter.com/calcs9/status/1371927192817569798?s=20)
- [https://twitter.com/PonderJaunt/status/1372072606912835585?s=20](https://twitter.com/PonderJaunt/status/1372072606912835585?s=20)

[Cyber Spotlight №2](https://oncyber.io/spotlight2)

## WHO:

- A collection of independent artists XRP community members and developers.

## WHAT:

- A NFT marketplace running on the XRPL Decentralized exchange. Using XRPL enabled technology such as Xumm, Coil, Paystring, and Interledger as a foundation; while exploring integrations with emerging technology

## WHEN:

- Current expectations are to develop a working proof of concept before August. Target date for gallery event is August 2021.

## WHERE:

- Online. Decentralized development cohort. Utilizing collaborative digital workplaces and p2p systems to remotely develop a project with a global team.

## WHY:

- Exploring NFTs on the XRPL based on currently ongoing standardized conversations will offer insight into the technical feasibility of a protocol amendment to Rippled allowing for native NFT functionality.

Supplementing current NFT conversations with a XRPL enabled option to compete with market places based on ERC standards with an added benefit of value interoperability, low cost transactions, near instant settlement, and semantic ontology.
